package main

import (
	"database/sql"
	"html/template"
	"log"
	"net/http"
)

var (
	headerHTML = `<!DOCTYPE html>
<html>
<head>
 <title>Assets</title>
</head>
<body>
`

	indexHTML = headerHTML + `
<h1>Asset tracking</h1>

<p>
  Tracking {{.NumArtifacts}} artifacts across {{.NumPods}} pods.
</p>

<p>
  <form action="" method="get">
    <input type="text" name="q" value="{{.SearchQuery}}" size="40" autofocus>
    <button type="submit">Search</button>
  </form>
</p>

{{if .QueryError}}
<p style="color:red;">
  Query error: {{.QueryError}}
</p>
{{end}}

<p>
  <small>
    Search artifacts using a whitespace-separated list of queries, each following the syntax
    <i>attribute</i> <i>&lt;operator&gt;</i> <i>value</i>. Attributes can refer to the artifact
    or to the pod it belongs to, and include <i>name</i>, <i>type</i>,
    <i>version</i> for package artifacts, <i>pod</i>, <i>namespace</i>,
    <i>digest</i> for pods and images. Operators are the normal SQL binary operators
    (=, !=, &lt;, &gt;, etc), with the addition of ~ to signify the LIKE operator.
  </small>
</p>

<hr>

{{if .Results}}
<h3>Artifacts</h3>

<table class="table">
 <thead>
  <tr>
   <th>NS</th>
   <th>Pod</th>
   <th>Image</th>
   <th>Type</th>
   <th>Name</th>
   <th>Version</th>
  </tr>
 </thead>
 <tbody>
{{range .Results}}
  <tr>
    <td>{{.PodNamespace}}</td>
    <td>
      <a href="?q=pod%3D{{.PodName}}">{{.PodName}}</a>
    </td>
    <td>
      <a href="?q=digest%3D{{.ImageDigest}}">{{.ImageDigest}}</a>
    </td>
    <td>{{.Artifact.Type}}</td>
    <td>
      <a href="?q=name%3D{{.Artifact.Name}}">{{.Artifact.Name}}</a>
    </td>
    <td>
      <a href="?q=name%3D{{.Artifact.Name}}+version%3D{{.Artifact.Version}}">{{.Artifact.Version}}</a>
    </td>
  </tr>
{{end}}
 </tbody>
</table>
{{end}}

</body>
</html>`

	indexTemplate = template.Must(template.New("index").Parse(indexHTML))
)

type httpAPI struct {
	db *sql.DB
}

func (h *httpAPI) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	numArtifacts, numPods, _ := getDbStats(h.db)

	var results []result
	var queryError string
	searchQuery := req.FormValue("q")

	if searchQuery != "" {
		var err error
		results, err = doQueryString(h.db, searchQuery)
		if err != nil {
			queryError = err.Error()
		}
	}

	w.Header().Set("Cache-Control", "no-store")
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	if err := indexTemplate.Execute(w, map[string]interface{}{
		"SearchQuery":  searchQuery,
		"QueryError":   queryError,
		"NumArtifacts": numArtifacts,
		"NumPods":      numPods,
		"Results":      results,
	}); err != nil {
		log.Printf("template error for %s: %v", req.URL.Path, err)
	}
}
