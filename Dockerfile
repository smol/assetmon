FROM docker.io/library/golang:1.22.5 as build
ADD . /src
RUN cd /src && \
    go generate ./... && \
    go build -ldflags="-extldflags=-static" -tags "sqlite_omit_load_extension netgo usergo" -o assetmon . && \
    strip assetmon

FROM scratch
COPY --from=build /src/assetmon /assetmon

ENTRYPOINT ["/assetmon"]

