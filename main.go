package main

import (
	"flag"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"golang.org/x/sync/errgroup"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/clientcmd"
	"k8s.io/klog/v2"

	// Load all auth plugins.
	_ "k8s.io/client-go/plugin/pkg/client/auth"
)

var (
	dburi      = flag.String("db", "/data/assetmon.db", "path to the database file")
	kubeconfig = flag.String("kubeconfig", os.Getenv("KUBECONFIG"), "absolute path to the kubeconfig file")
	master     = flag.String("master", "", "master url")
	httpAddr   = flag.String("http-addr", ":5972", "address for the debug HTTP console")
)

func main() {
	flag.Parse()

	db, err := openDB(*dburi)
	if err != nil {
		klog.Fatal(err)
	}
	defer db.Close()

	// Create the connection.
	config, err := clientcmd.BuildConfigFromFlags(*master, *kubeconfig)
	if err != nil {
		klog.Fatal(err)
	}

	// Create the clientset.
	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		klog.Fatal(err)
	}

	// Create a stop channel to control termination via signal.
	stop := make(chan struct{})
	sigCh := make(chan os.Signal, 1)
	go func() {
		<-sigCh
		klog.Info("received termination signal, exiting...")
		close(stop)
	}()
	signal.Notify(sigCh, syscall.SIGTERM, syscall.SIGINT)

	// Run the workqueue, the scanner, and the HTTP server in an
	// errgroup, so that we can safely terminate all of them on error.
	var g errgroup.Group

	g.Go(func() error {
		runWorkqueue(clientset, newPodSyncTarget(db), stop)
		return nil
	})

	g.Go(func() error {
		return runScanner(db, stop)
	})

	g.Go(func() error {
		srv := &http.Server{
			Addr:              *httpAddr,
			Handler:           &httpAPI{db: db},
			ReadHeaderTimeout: 30 * time.Second,
		}
		go func() {
			<-stop
			srv.Close()
		}()
		return srv.ListenAndServe()
	})

	if err := g.Wait(); err != nil {
		klog.Fatal(err)
	}
}
