package main

import (
	"context"
	"database/sql"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"time"

	"git.autistici.org/ai3/go-common/sqlutil"
	v1 "k8s.io/api/core/v1"
	"k8s.io/klog/v2"
)

var tmpDir = flag.String("tmp-dir", "", "temporary directory for container scans (default /tmp)")

type Artifact struct {
	Type    string `json:"type"`
	Name    string `json:"name"`
	Version string `json:"version"`
}

// Upon receiving a Pod update: Scan ContainerStatuses for image IDs,
// emit a pod/container tuple for each. Immediately update the
// pod->container map in the database. Queue a scan of the container
// if we haven't seen it yet. Scanning can take a (relatively) long
// time, so decouple it from the Kubernetes watcher using a
// database-backed queue.

type podSyncTarget struct {
	db *sql.DB
}

func newPodSyncTarget(db *sql.DB) *podSyncTarget {
	return &podSyncTarget{db: db}
}

func (t *podSyncTarget) HandleAddUpdatePod(pod *v1.Pod) error {
	return sqlutil.WithTx(context.Background(), t.db, func(tx *sql.Tx) error {
		for _, status := range pod.Status.ContainerStatuses {
			if !status.Ready {
				continue
			}

			klog.Infof("pod %s/%s: image %s", pod.GetNamespace(), pod.GetName(), status.ImageID)
			isNew, err := addImageToDatabaseIfNotExist(tx, pod.GetNamespace(), pod.GetName(), status.ImageID)
			if err != nil {
				return err
			}
			if isNew {
				if err := addImageToScanQueue(tx, status.ImageID); err != nil {
					return err
				}
			}
		}
		return nil
	})
}

func (t *podSyncTarget) HandleDeletePod(name string) error {
	return sqlutil.WithTx(context.Background(), t.db, func(tx *sql.Tx) error {
		return deletePodFromDatabase(tx, name)
	})
}

func runScanner(db *sql.DB, stop chan struct{}) error {
	// Poll the queue every N seconds, this acts both as an
	// intrinsic rate-limiter and as a way to introduce delays in
	// what is otherwise an i/o intensive process.
	ticker := time.NewTicker(5 * time.Second)
	defer ticker.Stop()

	for {
		select {
		case <-stop:
			log.Printf("stopping scan worker")
			return nil
		case <-ticker.C:
		}

		imageID, ok, err := popScanQueue(db)
		if err != nil {
			return err
		}
		if !ok {
			continue
		}

		//  Actually run the scan. Errors here should not stop
		//  runScanner(), we'll just log them. Only fail
		//  permanently on database errors.
		artifacts, err := scanImage(imageID)
		if err != nil {
			klog.Errorf("error scanning image %s: %v", imageID, err)
		} else {
			if err := saveArtifacts(db, imageID, artifacts); err != nil {
				return err
			}
		}

		if err := markScanAsDone(db, imageID); err != nil {
			return err
		}
	}
}

// Type used to parse the syft JSON output.
type syftResult struct {
	Artifacts []Artifact `json:"artifacts"`
	Distro    struct {
		Name    string `json:"name"`
		Version string `json:"version"`
	} `json:"distro"`
}

func cmd(cmd string, args ...string) *exec.Cmd {
	c := exec.Command(cmd, args...)
	c.Stderr = os.Stderr
	return c
}

func scanImage(imageID string) ([]Artifact, error) {
	dir, err := os.MkdirTemp(*tmpDir, "image-tmp-*")
	if err != nil {
		return nil, err
	}
	defer os.RemoveAll(dir)

	log.Printf("scanning image %s in %s", imageID, dir)
	imagePath := filepath.Join(dir, "image")

	if err := cmd("skopeo", "copy", "docker://"+imageID, fmt.Sprintf("oci-archive:%s:image", imagePath)).Run(); err != nil {
		return nil, fmt.Errorf("failed to download image: %w", err)
	}

	c := cmd("env", "SYFT_CHECK_FOR_APP_UPDATE=false", "syft", "scan", "--quiet", "-o", "json", fmt.Sprintf("oci-archive:%s", imagePath))
	stdout, err := c.StdoutPipe()
	if err != nil {
		return nil, fmt.Errorf("failed to create stdout pipe for scan: %w", err)
	}
	if err := c.Start(); err != nil {
		return nil, fmt.Errorf("failed to scan image: %w", err)
	}

	var output syftResult
	if err := json.NewDecoder(stdout).Decode(&output); err != nil {
		return nil, fmt.Errorf("failed to scan image: JSON error: %w", err)
	}

	if err := c.Wait(); err != nil {
		return nil, fmt.Errorf("failed to scan image: %w", err)
	}

	// If present, turn the 'distro' information into a pseudo-artifact.
	if output.Distro.Name != "" {
		output.Artifacts = append(output.Artifacts, Artifact{
			Type:    "distro",
			Name:    output.Distro.Name,
			Version: output.Distro.Version,
		})
	}

	return output.Artifacts, nil
}
