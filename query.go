package main

import (
	"bufio"
	"bytes"
	"context"
	"database/sql"
	"errors"
	"fmt"
	"io"
	"strings"

	"git.autistici.org/ai3/go-common/sqlutil"
)

type token int

const (
	ILLEGAL token = iota
	EOF

	// Literals
	IDENTIFIER

	// Operators
	OPERATOR
)

var eof = rune(0)

func isWhitespace(ch rune) bool {
	return ch == ' ' || ch == '\t' || ch == '\n'
}

// Query string scanner (lexer).
type scanner struct {
	r *bufio.Reader
}

func newScanner(r io.Reader) *scanner {
	// Optimized for small strings.
	return &scanner{r: bufio.NewReaderSize(r, 128)}
}

func (s *scanner) read() rune {
	ch, _, err := s.r.ReadRune()
	if err != nil {
		return eof
	}
	return ch
}

func (s *scanner) unread() {
	_ = s.r.UnreadRune()
}

func (s *scanner) Scan() (token, string) {
	// Skip over whitespace.
	var ch rune
	for {
		ch = s.read()
		if ch == eof || !isWhitespace(ch) {
			break
		}
	}

	switch ch {
	case eof:
		return EOF, ""
	case '=', '<', '>':
		return OPERATOR, string(ch)
	case '"':
		// Do not unread the quote.
		return s.scanQuotedIdent()
	}

	// Everything else is an identifier.
	s.unread()
	return s.scanIdent()
}

func (s *scanner) scanIdent() (token, string) {
	var buf bytes.Buffer
	buf.WriteRune(s.read())

	for {
		if ch := s.read(); ch == eof {
			break
		} else if isWhitespace(ch) || ch == '=' {
			s.unread()
			break
		} else {
			_, _ = buf.WriteRune(ch)
		}
	}

	return IDENTIFIER, buf.String()
}

func (s *scanner) scanQuotedIdent() (token, string) {
	var buf bytes.Buffer
	buf.WriteRune(s.read())

	for {
		if ch := s.read(); ch == eof || ch == '"' {
			break
		} else {
			_, _ = buf.WriteRune(ch)
		}
	}

	return IDENTIFIER, buf.String()
}

// A simple query string parser that supports attribute=value metadata
// matches and quoted strings.
type queryStringParser struct {
	s   *scanner
	buf struct {
		tok token
		lit string
		n   int
	}
}

type queryPart struct {
	field string
	value string
	op    string
}

type query []queryPart

var (
	errEOF    = errors.New("eof")
	errSyntax = errors.New("syntax error")
)

func newQueryStringParser(r io.Reader) *queryStringParser {
	return &queryStringParser{s: newScanner(r)}
}

func (p *queryStringParser) scan() (tok token, lit string) {
	if p.buf.n != 0 {
		p.buf.n = 0
		return p.buf.tok, p.buf.lit
	}

	tok, lit = p.s.Scan()
	p.buf.tok, p.buf.lit = tok, lit
	return
}

func (p *queryStringParser) unscan() { p.buf.n = 1 }

func (p *queryStringParser) scanQueryPart() (part queryPart, err error) {
	tok, lit := p.scan()
	if tok == EOF {
		err = errEOF
		return
	} else if tok != IDENTIFIER {
		err = errSyntax
		return
	}
	// Peek for an equals sign.
	nextTok, op := p.scan()
	if nextTok != OPERATOR {
		part.value = lit
		p.unscan()
		return
	}
	// Ingest another identifier.
	part.field = lit
	part.op = op
	tok, lit = p.scan()
	if tok != IDENTIFIER {
		err = errSyntax
	}
	part.value = lit
	return
}

func (p *queryStringParser) parse() (query, error) {
	var parsed []queryPart

	// A query string is just a list of field=value matches that
	// are then logically ANDed.
	for {
		part, err := p.scanQueryPart()
		if err == errEOF {
			break
		} else if err != nil {
			return nil, err
		}
		parsed = append(parsed, part)
	}

	return parsed, nil
}

// Map query string fields to SQL query fields.
var (
	defaultFields = []string{"i.image_digest", "p.name", "a.name"}
	queryFieldMap = map[string]string{
		"digest":    "i.image_digest",
		"pod":       "p.name",
		"namespace": "p.namespace",
		"name":      "a.name",
		"version":   "a.version",
	}
)

func (q query) toSQL() (string, []any, error) {
	var clauses []string
	var args []any

	for _, part := range q {
		if part.field == "" {
			var subclauses []string
			for _, df := range defaultFields {
				subclauses = append(subclauses, fmt.Sprintf("%s = ?", df))
				args = append(args, part.value)
			}
			clauses = append(clauses, fmt.Sprintf("(%s)", strings.Join(subclauses, " OR ")))
		} else if f, ok := queryFieldMap[part.field]; ok {
			clauses = append(clauses, fmt.Sprintf("%s %s ?", f, part.op))
			args = append(args, part.value)
		} else {
			return "", nil, fmt.Errorf("unknown field %s", part.field)
		}
	}

	return strings.Join(clauses, " AND "), args, nil
}

type result struct {
	Artifact

	ImageDigest string

	PodNamespace string
	PodName      string
}

func doQueryString(db *sql.DB, qstr string) ([]result, error) {
	q, err := newQueryStringParser(strings.NewReader(qstr)).parse()
	if err != nil {
		return nil, err
	}

	where, args, err := q.toSQL()
	if err != nil {
		return nil, err
	}

	return doQuery(db, where, args)
}

func doQuery(db *sql.DB, where string, args []any) ([]result, error) {
	var results []result
	err := sqlutil.WithReadonlyTx(context.Background(), db, func(tx *sql.Tx) error {
		q := fmt.Sprintf(`
			SELECT
			  a.name, a.type, a.version, a.image_digest, p.namespace, p.name
			FROM
			  artifacts AS a
			  JOIN images AS i ON a.image_digest = i.image_digest
			  JOIN pod_images AS pi ON i.image_digest = pi.image_digest
			  JOIN pods AS p ON pi.pod = p.qualified_name
			WHERE %s
			ORDER BY p.namespace ASC, p.name ASC, a.image_digest ASC, a.name ASC
`, where)
		rows, err := tx.Query(q, args...)
		if err != nil {
			return nil
		}
		defer rows.Close()

		for rows.Next() {
			var r result
			if err := rows.Scan(
				&r.Artifact.Name,
				&r.Artifact.Type,
				&r.Artifact.Version,
				&r.ImageDigest,
				&r.PodNamespace,
				&r.PodName,
			); err != nil {
				return err
			}
			results = append(results, r)
		}

		return rows.Err()
	})

	return results, err
}
