package main

import (
	"context"
	"database/sql"
	"errors"
	"fmt"

	"git.autistici.org/ai3/go-common/sqlutil"
)

func qualifiedName(ns, name string) string {
	return fmt.Sprintf("%s/%s", ns, name)
}

var migrations = []func(*sql.Tx) error{
	sqlutil.Statement(`
CREATE TABLE pods (
    qualified_name TEXT NOT NULL,
    namespace TEXT NOT NULL,
    name TEXT NOT NULL,
    seen DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (qualified_name)
);
`, `
CREATE UNIQUE INDEX idx_pods_name_unique ON pods(namespace, name);
`, `
CREATE TABLE images (
    image_digest TEXT NOT NULL,
    distro TEXT,
    distro_version TEXT,
    PRIMARY KEY (image_digest)
);
`, `
CREATE TABLE pod_images (
    pod TEXT NOT NULL,
    image_digest TEXT NOT NULL,
    CONSTRAINT fk_pod_images_pod FOREIGN KEY (pod) REFERENCES pods(qualified_name) ON DELETE CASCADE,
    CONSTRAINT fk_pod_images_digest FOREIGN KEY (image_digest) REFERENCES images(image_digest) ON DELETE CASCADE
);
`, `
CREATE UNIQUE INDEX idx_pod_images ON pod_images(pod, image_digest);
`, `
CREATE INDEX idx_pod_images_pod ON pod_images(pod);
`, `
CREATE INDEX idx_pod_images_digest ON pod_images(image_digest);
`, `
CREATE TABLE artifacts (
    image_digest TEXT NOT NULL,
    name TEXT NOT NULL,
    type TEXT NOT NULL,
    version TEXT NOT NULL,
    CONSTRAINT fk_artifacts_image FOREIGN KEY (image_digest) REFERENCES images(image_digest) ON DELETE CASCADE
);
`, `
CREATE INDEX idx_artifacts_images_digest ON artifacts(image_digest);
`, `
CREATE INDEX idx_artifacts_name_type ON artifacts(name, type);
`, `
CREATE TABLE scan_queue (
    image_digest TEXT NOT NULL,
    leased BOOLEAN NOT NULL DEFAULT 0,
    timestamp DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (image_digest),
    CONSTRAINT fk_scan_queue_image FOREIGN KEY (image_digest) REFERENCES images(image_digest) ON DELETE CASCADE
);
`),
}

// openDB opens a SQLite database and runs the database migrations.
func openDB(dburi string) (*sql.DB, error) {
	return sqlutil.OpenDB(dburi, sqlutil.WithMigrations(migrations))
}

func addImageToScanQueue(tx *sql.Tx, imageID string) error {
	_, err := tx.Exec("INSERT INTO scan_queue (image_digest) VALUES (?)", imageID)
	return err
}

func popScanQueue(db *sql.DB) (imageID string, ok bool, err error) {
	err = sqlutil.WithTx(context.Background(), db, func(tx *sql.Tx) error {
		err := tx.QueryRow("SELECT image_digest FROM scan_queue WHERE leased = ? LIMIT 1", false).Scan(&imageID)
		if errors.Is(err, sql.ErrNoRows) {
			err = nil
		} else if err == nil {
			ok = true
			_, err = tx.Exec("UPDATE scan_queue SET leased = ? WHERE image_digest = ?", true, imageID)
		}
		return err
	})

	return
}

func markScanAsDone(db *sql.DB, imageID string) error {
	return sqlutil.WithTx(context.Background(), db, func(tx *sql.Tx) error {
		_, err := tx.Exec("DELETE FROM scan_queue WHERE image_digest = ?", imageID)
		return err
	})
}

func addPodToDatabase(tx *sql.Tx, ns, name string) error {
	_, err := tx.Exec(
		"INSERT INTO pods (qualified_name, namespace, name) VALUES (?, ?, ?)",
		qualifiedName(ns, name), ns, name)
	return err
}

func deletePodFromDatabase(tx *sql.Tx, qualifiedName string) error {
	orphanedImages, err := getOrphanedImages(tx, qualifiedName)
	if err != nil && !errors.Is(err, sql.ErrNoRows) {
		return err
	}

	if _, err := tx.Exec("DELETE FROM pods WHERE qualified_name = ?", qualifiedName); err != nil {
		return err
	}

	for _, imageID := range orphanedImages {
		if _, err := tx.Exec("DELETE FROM images WHERE image_digest = ?", imageID); err != nil {
			return err
		}
	}

	return nil
}

func getPodImages(tx *sql.Tx, ns, name string) ([]string, error) {
	rows, err := tx.Query("SELECT image_digest FROM pod_images WHERE pod = ?", qualifiedName(ns, name))
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var images []string
	for rows.Next() {
		var imageID string
		if err := rows.Scan(&imageID); err != nil {
			return nil, err
		}
		images = append(images, imageID)
	}
	return images, rows.Err()
}

func getOrphanedImages(tx *sql.Tx, qualifiedName string) ([]string, error) {
	// Which images will be left with refcount=0 after removing the specified pod?
	rows, err := tx.Query(`
		SELECT
		  image_digest, COUNT(*) AS c
		FROM pod_images
		WHERE
		  image_digest IN (SELECT image_digest FROM pod_images WHERE pod = ?) AND pod != ?
		GROUP BY image_digest
		HAVING c = 0
		`, qualifiedName, qualifiedName)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var images []string
	for rows.Next() {
		var imageID string
		var zero int
		if err := rows.Scan(&imageID, &zero); err != nil {
			return nil, err
		}
		images = append(images, imageID)
	}
	return images, rows.Err()
}

func addPodToDatabaseIfNotExist(tx *sql.Tx, ns, name string) error {
	qname := qualifiedName(ns, name)

	var n int
	if err := tx.QueryRow("SELECT 1 FROM pods WHERE qualified_name = ?", qname).Scan(&n); err == nil && n == 1 {
		return nil
	}

	_, err := tx.Exec("INSERT INTO pods (qualified_name, namespace, name) VALUES (?, ?, ?)", qname, ns, name)
	return err
}

func addImageToDatabaseIfNotExist(tx *sql.Tx, ns, name, imageID string) (bool, error) {
	if err := addPodToDatabaseIfNotExist(tx, ns, name); err != nil {
		return false, err
	}

	isNew := false
	var n int
	if err := tx.QueryRow("SELECT 1 FROM images WHERE image_digest = ?", imageID).Scan(&n); err != nil || n != 1 {
		if _, err := tx.Exec("INSERT INTO images (image_digest) VALUES (?)", imageID); err != nil {
			return false, err
		}
		isNew = true
	}

	_, err := tx.Exec(
		"INSERT OR IGNORE INTO pod_images (pod, image_digest) VALUES (?, ?)",
		qualifiedName(ns, name),
		imageID)

	return isNew, err
}

func saveArtifacts(db *sql.DB, imageID string, artifacts []Artifact) error {
	return sqlutil.WithTx(context.Background(), db, func(tx *sql.Tx) error {
		for _, a := range artifacts {
			_, err := tx.Exec(
				"INSERT INTO artifacts (image_digest, name, type, version) VALUES (?, ?, ?, ?)",
				imageID, a.Name, a.Type, a.Version,
			)
			if err != nil {
				return err
			}
		}
		return nil
	})
}

func getDbStats(db *sql.DB) (numArtifacts int, numPods int, err error) {
	err = sqlutil.WithReadonlyTx(context.Background(), db, func(tx *sql.Tx) error {
		if err := tx.QueryRow("SELECT COUNT(*) FROM artifacts").Scan(&numArtifacts); err != nil {
			return err
		}
		return tx.QueryRow("SELECT COUNT(*) FROM pods").Scan(&numPods)
	})

	return
}
